class Scrapper
  require 'redis'
  require_relative './downloader'
  require_relative './transformer'
  include Transformer

  REDIS = Redis.new

  attr_reader :base_uri, :link

  def initialize(url)
    @base_uri = 'http://music.я.wiki'
    @link = url
  end

  def validate_artist
    ['Популярные исполнители', '', 'Лорак Ани', 'Наталия Власова'] # Убираем название из общего списка, и исполнителей с битыми ссылками
  end

  def artists(link)
    doc(link).css('.gl__simplyblock').css('a[href^="/artist"]')
  end

  def songs(artist)
    doc("#{base_uri}#{artist['href']}").css('li.track')
  end

  def scrap
    artists(link).each do |artist|
      main_artist = artist.inner_text.strip
      next if validate_artist.include?(main_artist)
      puts "\t\tArtist: #{main_artist}"

      songs(artist).each do |song|
        artist_name = song.css('h2.playlist-name b').inner_text.strip
        title = song.css('h2.playlist-name em').inner_text.strip
        track = song.css('a.playlist-btn-down.no-ajaxy').first['href']
        track_uri = (normalize(base_uri) + track)
        songs_hash = Digest::MD5.hexdigest("#{artist_name} - #{title}")
        full_title = [title, artist_name]

        next if   REDIS.exists(songs_hash)

        Downloader.start(track_uri, songs_hash, full_title)
      end
    end
  end
end
