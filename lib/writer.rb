module Writer
  def write(f, easy, songs_hash, full_title)
    if (f.write easy.body_str) && !File.zero?('./tmp/tmp.mp3')
      Scrapper::REDIS.set(songs_hash, {title: full_title[0], artist_name: full_title[1]})

      `mv ./tmp/tmp.mp3 ./files/#{songs_hash}.mp3`

      track = "./tmp/#{songs_hash}.mp3"
      saved_song = eval(Scrapper::REDIS.get(songs_hash))

      # `curl -v -F "uploader_key=petr" -F "artist=#{saved_song[:artist_name]}" -F "song=#{saved_song[:title]}" -F "file=@#{track}" http://82.202.167.177/tracks`

      puts "\tЗаписано в #{Time.now.strftime('%H:%M:%S')} => #{saved_song[:title]}: #{saved_song[:artist_name]}"

      # `rm ./tmp/#{songs_hash}.mp3`
    else
      puts 'Не удалось загрузить файл'
    end
  end
end
