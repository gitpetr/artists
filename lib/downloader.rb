class Downloader
  require 'curb'
  require 'timeout'
  require_relative './writer'
  extend Writer

  def self.start(track_uri, songs_hash, full_title)
    easy = Curl::Easy.new
    easy.follow_location = true
    easy.url = track_uri

    File.open('./tmp/tmp.mp3', 'wb') do |f|
      print '................ '

      begin # во всем блоке одно действие: easy.perform, повторяется при ошибке соединения
        sleep(3)
        begin
          Timeout.timeout(600) do
            easy.perform
          end
        rescue Timeout::Error
          puts "\t\t13: Ошибка ответа"
        end

      rescue Curl::Err::ConnectionFailedError => e
        File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
        puts e.inspect
        sleep(30)
        puts "\t\t1: Reconnect"
        begin
          easy.perform
        rescue Curl::Err::ConnectionFailedError => e
          File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
          puts e.inspect
          sleep(30)
          puts "\t\t2: Reconnect"
          begin
            easy.perform
          rescue Curl::Err::ConnectionFailedError => e
            File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
            puts e.inspect
            sleep(30)
            puts "\t\t3: Не удалось загрузить"
          rescue Exception => e
            File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
            puts e.inspect
            sleep(30)
            puts "\t\t3.1: Next"
          end
        rescue Exception => e
          File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
          puts e.inspect
          sleep(30)
          puts "\t\t2.1: Next"
        end
      rescue Exception => e
        File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
        puts e.inspect
        sleep(30)
        puts "\t\t1.1: Next"
      end

      if (easy.response_code == 200) && (easy.content_type[0..4] == 'audio')
        write(f, easy, songs_hash, full_title)
      else
        puts "\t\t4: пропуск, response_code: #{easy.response_code}, content_type: #{easy.content_type}"
      end
    end
  end
end
