class ScrapperForLetter < Scrapper
  attr_accessor :letter_links, :link
  attr_reader :base_uri

  def initialize(url)
    @base_uri = 'http://music.я.wiki'
    @letter_links = letters(url)
    @link = ''
  end

  def letters(link)
    link_letters = doc(link).css('.foo-letters a[href^="/art"]')[1..-1]
    link_letters.map { |letter| letter['href'] }
  end

  def forth
    letter_links.each do |tail_link|
      self.link = "#{base_uri}#{tail_link}"
      scrap
    end
  end
end
