module Transformer
  require 'nokogiri'
  require 'open-uri'
  require 'addressable/uri'

  def normalize(uri)
    Addressable::URI.parse(uri).normalize
  end

  def doc(uri)
    link = normalize(uri)
    puts 'parsing'

    begin # во всем блоке одно действие: @html = open(link), переподключение при ошибке
      Timeout.timeout(30) do
        @html = open(link)
      end
    rescue Timeout::Error
      puts "\t\t6: Ошибка ответа"
    rescue OpenURI::HTTPError => e
      File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
      puts e.message, e.inspect
      puts "\t\t7: reopen"
      sleep(30)
      begin
        Timeout.timeout(30) do
          @html = open(link)
        end
      rescue Timeout::Error
        puts "\t\t8: Ошибка ответа"
      rescue OpenURI::HTTPError => e
        File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
        puts e.message, e.inspect
        puts "\t\t9: reopen"
        sleep(60)
        begin
          Timeout.timeout(30) do
            @html = open(link)
          end
        rescue Timeout::Error
          puts "\t\t10: Ошибка ответа"
        rescue OpenURI::HTTPError => e
          File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
          puts e.message, e.inspect
          puts "\t\t11: reopen"
          sleep(180)
          begin
            Timeout.timeout(30) do
              @html = open(link)
            end
          rescue Timeout::Error
            puts "\t\t12: Ошибка ответа"
          rescue OpenURI::HTTPError => e
            File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
            puts '13:', e.message, e.inspect
          end
        rescue Exception => e
          File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
          puts e.message, e.inspect
          puts "\t\t7.1: next"
          sleep(30)
        end
      rescue Exception => e
        File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
        puts e.message, e.inspect
        puts "\t\t7.1: next"
        sleep(30)
      end
    rescue Exception => e
      File.open('./errors.log', 'a') { |f| f.write "#{e.backtrace}: \n#{e.message} \n#{e.inspect}" }
      puts e.message, e.inspect
      puts "\t\t7.1: next"
      sleep(30)
    end

    Nokogiri::HTML(@html.read)
  end
end
