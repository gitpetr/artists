class RedisClear

  def self.clear
    Redis.new.flushall
  end
end